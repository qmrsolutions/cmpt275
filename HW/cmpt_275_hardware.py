#File name: cmpt_275_hardware
#Author: Dana Sy
#Project Group 6
#QMRS Solutions

#changes: Microcontroller is able to POST JSON to server - 11/5/2016

#bugs: no known bugs
#testing was done functionally as code is very short

import RPi.GPIO as GPIO
import time
import requests
import json

#set up pins for raspberry pi

IN_PIR = 37
OUT_PIR = 35
LED = 3
GPIO.setmode(GPIO.BOARD)
GPIO.setup(IN_PIR, GPIO.IN)
GPIO.setup(OUT_PRI, GPIO.IN)
GPIO.setup(LED,GPIO.OUT)

# data to fill up the JSON requests
leave = { 'lot' : 'a', 'input': '0' }
enter = { 'lot' : 'a', 'input': '1' }
header = {'content-type': 'application/json'}
url = 'https://httpbin.org/post'

try:
    while True:
        # read sensor values
        in_sensor = GPIO.input(IN_PIR)
        out_sensor = GPIO.input(OUT_PIR)

        # car tripped sensor for in
        if in_sensor == 1:
            print("something entered")
            #GPIO.output(LED,GPIO.HIGH)
            resp = requests.post(url, data=json.dumps(enter), headers=header)
            if resp.status_code != 200:
                print("Unable to post data to server!")
            else:
                print("posted to server")
                print(json.dumps(resp.json(), indent=4))
        # car tripped sensor for out
        if out_sensor == 1:
            print("something left")
            #GPIO.output(LED,GPIO.HIGH)
            resp = requests.post(url, data=json.dumps(leave), headers=header)
            if resp.status_code != 200:
                print("Unable to post data to server!")
            else:
                print("posted to server")
                print(json.dumps(resp.json(), indent=4))
        # check again in 1/10 of a second, might want to modify to use edge trigger
        time.sleep(1)
#clean up GPIO on keyboard interrupt
except KeyboardInterrupt:
    GPIO.cleanup()

#File name: ParkSFUserver.py
#Author: Randeep Shahi
#Project Group 6
#QMRS Solutions

#changes: basic JSON functionality is implemented - 11/5/2016

#bugs: no known bugs
#testing was done using POSTMAN app on Ubuntu

import os
import json
import requests
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from contextlib import closing

# create our little application :)
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'ggFinder.db'),
    DEBUG=False,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

@app.route('/lot_information', methods=['GET', 'POST'])
def lot_information():
    if request.json:
        jsondata = request.json
        print jsondata

        if jsondata.get("lot a") == "200": #authentication code
            with open('data.json', 'r') as f:
                json_data = json.load(f)
                string1 = json_data['parking lots'][0]['free']
                string2 = json_data['parking lots'][0]['max']

                list =      {"free": string1, "max": string2,}

            return jsonify(Lot_A=list)

        if jsondata.get("lot b") == "100": #authentication code
            with open('data.json', 'r') as f:
                json_data = json.load(f)
                string1 = json_data['parking lots'][1]['free']
                string2 = json_data['parking lots'][1]['max']

                list =      {"free": string1, "max": string2,}

            return jsonify(Lot_B=list)

        else:
            return 'failed'
    else:
        return 'empty request'
#currenlty only handles parking lot 'lot a'
@app.route('/update_parking_lot_information', methods=['GET', 'POST'])
def update_parking():
    if request.json:
        jsondata = request.json
        print jsondata

        if jsondata.get("lot") == "a":
            if jsondata.get("input") == "0":
                #update local json file
                with open('data.json', 'r') as f:
                    json_data = json.load(f)
                    number =  json_data['parking lots'][0]['free']
                    newValue = int(number) - 1
                    json_data['parking lots'][0]['free'] = str(newValue)

                with open('data.json', 'w') as f:
                    f.write(json.dumps(json_data))

                print "a car has left the lot"
                return "success"


            elif jsondata.get("input") == "1":
                #update local json file
                with open('data.json', 'r') as f:
                    json_data = json.load(f)
                    number =  json_data['parking lots'][0]['free']
                    newValue = int(number) + 1
                    json_data['parking lots'][0]['free'] = str(newValue)

                with open('data.json', 'w') as f:
                    f.write(json.dumps(json_data))

                print "a car has left the lot"
                return "success"
                #update local json file

    else:
        return "invalid information"

#to be implement in prototype 2
@app.route('/add_report', methods=['GET', 'POST'])
def add_report():
    if request.json:
        jsondata = request.json
        print jsondata
        #still need to check that a valid report is being

        with open('reports.json', 'r') as f:
            json_data = json.load(f)

            json_data.append(jsondata)

        with open('reports.json', 'w') as f:
            f.write(json.dumps(json_data))

        return "success"
    else:
        return "invalid report submission"

@app.route('/data.json', methods=['GET', 'POST'])
def dataJson():
    with open('data.json', 'r') as f:
        json_data = json.load(f)
        print "hello"
        print json_data
        return jsonify(json_data)

@app.route('/reports')
def show_reports():

    print "before exe"

    with open('reports.json', 'r') as f:
        json_data = json.load(f)
        maptest = map(list, json_data)

        test = jsonify(json_data)
        print test

        return render_template('show_reports.html', reports=maptest)
        #return jsonify(reports=json_data)


if __name__ == '__main__':
    app.run(host='0.0.0.0')

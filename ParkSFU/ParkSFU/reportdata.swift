//
//  reportdata.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Class: This class contains variable from reportdata
//  Created by MOLLY BIN on 2016-10-29.
//  Edited by Molly Bin, Qassam Yomok
//  Bugs: no known bugs
//
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit

class reportdata {
    var Title:String = ""
    var Descrip: String = ""
    var Date: String = ""
    var Location: String = ""
    var Images: UIImageView!
}
//
//  BurnabyViewController.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Class: This class shows the burnaby campus and will demonstrate all the parking lots information
//         and the road condition for burnaby. It will have a button which link to the direction 
//         of a specific parking lot
//  Created by MOLLY BIN on 2016-10-29.
//  Edited by Molly Bin, Qassam Yomoks, Dana Sy
//
//  Bugs: no known bug
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit

class BurnabyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //define the variables in the navigation bar
    @IBOutlet weak var backtoHome: UIButton!
    @IBOutlet weak var navtitle: UINavigationItem!
    
    //define the variables in the roadreport
    @IBOutlet weak var BurnabyRoad: UILabel!
    @IBOutlet weak var BurnabyRoadCond: UILabel!
    @IBOutlet weak var AdjacentRoad: UILabel!
    @IBOutlet weak var AdjacentRoadCond: UILabel!
    @IBOutlet weak var ClassExam: UILabel!
    @IBOutlet weak var ClassExamCond: UILabel!
    @IBOutlet weak var TranslinkBuses: UILabel!
    @IBOutlet weak var TranslinkBusesCond: UILabel!
    
 
    //define the variables in the table view
    @IBOutlet weak var ParkingLotsTableView: UITableView!
    let sfudatahubURL = "http://www.sfu.ca/security/sfuroadconditions/api/2/current"
    var roadreportdata = RoadReportData()
    var parkinglotdata = ParkingLotData()
    var parkinglotsname = [String]()
    var parkinglotsfree = [String]()
    var parkinglotsgps = [String]()
    var EmergencyNote = ""
    var Availability = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()

        ParkingLotsTableView.delegate = self
        ParkingLotsTableView.dataSource = self
        // Do any additional setup after loading the view.
        setRoadReport()
        setEmergencyAlertView()
    

    }
    
    

    
    func setEmergencyAlertView(){
        if(Availability == "true"){
            let alert = UIAlertController(title: "Emergency Notification!", message: EmergencyNote, preferredStyle: UIAlertControllerStyle.Alert)
            let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) { (ok) -> Void in
                print("tap ok!")}
            self.presentViewController(alert, animated: true, completion: nil)
             alert.addAction(ok)
        }else{
                print("no emergency")
            }
        
        
    }

    func setRoadReport(){
        BurnabyRoadCond.text = roadreportdata.broad
        AdjacentRoadCond.text = roadreportdata.adjroad
        ClassExamCond.text = roadreportdata.classexam
        TranslinkBusesCond.text = roadreportdata.transbus
        print(roadreportdata.broad)
    }
    
    /*-----------------------------Table View Delegate functions----------------------------*/
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return parkinglotdata.lotName.count
       
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "parkingLotsCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        cell.textLabel?.text = parkinglotdata.lotName[indexPath.row]
        cell.detailTextLabel?.text = parkinglotdata.FreeSpot[indexPath.row]


        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)

        let cellgps = parkinglotdata.GPS[indexPath.row]
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.google.com/maps?saddr=Current+Location&daddr="+cellgps)!)
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

    }*/


}

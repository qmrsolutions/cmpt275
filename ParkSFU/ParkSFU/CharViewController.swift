//
//  CharViewController.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Class: This class will illustrate all the data in a chart view
//  Created by MOLLY BIN on 2016-11-05.
//  Edited by Molly Bin, Qassam Yomok
//  Bugs: no known bugs
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit
import JBChart

class CharViewController: UIViewController, JBBarChartViewDelegate, JBBarChartViewDataSource {

    @IBOutlet weak var infoLable: UILabel!
    @IBOutlet weak var BarChart: JBBarChartView!
    
    var statData = StatisticData()
    var charData = [Int]()
    var charLegend = ["00 am","01 am","02 am","03 am","04 am","05 am","06 am","07 am","08 am ","09 am","10 am","11 am","12 pm","13 pm","14 pm","15 pm","16 pm","17 pm","18 pm","19 pm ","20 pm","21 pm","22 pm","23 pm"]
    
    
    //var charData = [30,32,32,33,40,70,100,120,122,125,135,140,150,150,150,145,130,100,95,96,85,65,55,45]
   // charData = statData.Data
    override func viewDidLoad() {
        super.viewDidLoad()
  
        charData = statData.Data
        view.backgroundColor = UIColor.darkGrayColor()
        BarChart.backgroundColor = UIColor.darkGrayColor()
        BarChart.delegate = self
        BarChart.dataSource = self
        BarChart.minimumValue = 0
        BarChart.maximumValue = 100
        
        // to-do header
        var footerView = UIView(frame: CGRectMake(0, 0, BarChart.frame.width, 16))
        
        var footer1 = UILabel(frame: CGRectMake(0, 0, BarChart.frame.width/2, 16))
        //footer1.textColor = UIColor.whiteColor()
        //footer1.text = "\(charLegend[0])"
        
       var footer2 = UILabel(frame: CGRectMake(0, 0, BarChart.frame.width/2, 16))
        //footer2.textColor = UIColor.whiteColor()
       // footer2.text = "\(charLegend[charLegend.count - 1])"
        //footer2.textAlignment = NSTextAlignment.Right
        
       footerView.addSubview(footer1)
       footerView.addSubview(footer2)
        
        var header = UILabel(frame: CGRectMake(0,0,BarChart.frame.width,50))
        header.textColor = UIColor.whiteColor()
        header.font = UIFont.systemFontOfSize(24)
        header.text = "Number of Cars Per hour"
        header.textAlignment = NSTextAlignment.Right
        BarChart.footerView = footerView
        BarChart.headerView = header
        
        
        
        BarChart.reloadData()
        BarChart.setState(.Collapsed, animated: false)        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        BarChart.reloadData()
        var timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("showChart"), userInfo: nil, repeats: false)
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        hideChart()
        
        
    }
    
    func hideChart(){
        BarChart.setState(.Collapsed, animated: true)
    }
    
    func showChart(){
        BarChart.setState(.Expanded, animated: true)
    }
    
    
    func numberOfBarsInBarChartView(barChartView: JBBarChartView!) -> UInt {
        return UInt(charData.count)
    }
    
    func barChartView(barChartView: JBBarChartView!, heightForBarViewAtIndex index: UInt) -> CGFloat {
        return CGFloat(charData[Int(index)])
    }
    func barChartView(barChartView: JBBarChartView!, colorForBarViewAtIndex index: UInt) -> UIColor! {
        return (index % 2 == 0) ? UIColor.lightGrayColor() : UIColor.whiteColor()
    }
    func barChartView(barChartView: JBBarChartView!, didSelectBarAtIndex index: UInt) {
        let data = charData[Int(index)]
        let key = charLegend[Int(index)]
        
        infoLable.text = "Number of Cars at \(key): is \(data)"
        
    }
    func didDeselectBarChartView(barChartView: JBBarChartView!) {
        infoLable.text = ""
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  httpProtocol.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Created by MOLLY BIN on 2016-11-02.
//  Edited by Molly Bin, Qassam Yomok
//  Bugs: no known bugs
//
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit

protocol httpProtocol {
    
    func didReResults(resultData: NSDictionary)
    
    
}
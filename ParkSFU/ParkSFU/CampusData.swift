//
//  CampusData.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Created by MOLLY BIN on 2016-11-05.
//  Edited by Molly Bin, Qassam Yomok
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit

class CampusData {
    var campus = ["Burnaby", "Vancouver", "Surrey"]
    var parkingLot = ["Burnaby": ["A Lot", "D Lot", "E Lot", "H Lot"],"Vancouver":["North Parking", "East Parking", "South Parking", "West Parking"],"Surrey":["P1","P2","P3"]]
}
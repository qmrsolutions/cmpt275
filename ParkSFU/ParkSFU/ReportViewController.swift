//
//  ReportViewController.swift
//  ParkSFU
//  Group 6: QMRSolution
//  Class: This reportViewController class is defined for users to send report
//         to sever. The report contains Title, Description, Date, Location,
//         and also images to be uploaded. It will combine the data information 
//         from reportdata and using JSON to communicate with sever.It will also 
//         alert users to confirm sending the report
//  Created by MOLLY BIN on 2016-10-29.
//  Edited by Molly Bin, Qassam Yomok, Dana Sy

////  Bugs: no known bugs
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit


class ReportViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    //define all the variables
    @IBOutlet weak var reporttitle: UITextField!
    
    @IBOutlet weak var reportdescp: UITextView!
    
    @IBOutlet weak var reportdate: UITextField!
    @IBOutlet weak var reportlocation: UITextField!
    @IBOutlet weak var selectedPics: UIImageView!
    let somereportdata = reportdata()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
  
    //click the image to selected the image from photolibrary
    @IBAction func toSelectPic(sender: AnyObject) {
        let ImagePicker = UIImagePickerController()
        ImagePicker.delegate = self
        ImagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(ImagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        selectedPics.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func SaveReportData(){
        somereportdata.Title = reporttitle.text!
        somereportdata.Descrip = reportdescp.text
        somereportdata.Date = reportdate.text!
        somereportdata.Location = reportlocation.text!
        somereportdata.Images = selectedPics
    }
    

    @IBAction func toSend(sender: AnyObject) {
     SaveReportData()
     print(somereportdata.Title)
     print(somereportdata.Descrip)
     print(somereportdata.Date)
     print(somereportdata.Location)
     print(somereportdata.Images.description)
        
        
     let alert = UIAlertController(title: "Upload Report", message: "Do you want to send the report to Security?", preferredStyle: UIAlertControllerStyle.Alert)
     let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) { (ok) -> Void in
        print("tap ok!")
        
        /* Send report to server
         * FIXME: use multipart/form-data to post image to server then use the response ID
         *        OR FIND SOME OTHER EASIER WAY
         * http://stackoverflow.com/questions/4083702/posting-a-file-and-data-to-restful-webservice-as-json
         */
        
        let unixTime = NSDate().timeIntervalSince1970
        let request = NSMutableURLRequest(URL: NSURL(string: "http://90bbb83d.ngrok.io/add_report")!)
        let session = NSURLSession.sharedSession()
        let jsonObj = [
            "Title":self.somereportdata.Title,
            "Desc":self.somereportdata.Descrip,
            "UserDate":self.somereportdata.Date,
            "Location":self.somereportdata.Location,
            "UnixTime":unixTime,
            "ImageName":self.somereportdata.Images.description
        ]
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(jsonObj, options: [])
        } catch let dataError {
            /* swift made me do this */
            print(dataError)
        }
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            if (statusCode != 200){
                print("ERROR posting to server")
            }
            /* Anything below just parses the JSON sent out not needed for the final version */
            let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Body: \(strData)")
            
        })
        
        task.resume()
        
        //clear all the report information after we send it and ready for the next report
        self.reporttitle.text = ""
        self.reportdescp.text = ""
        self.reportdate.text = ""
        self.reportlocation.text = ""
        self.selectedPics.image = UIImage(named: "Google Images")
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        self.presentViewController(alert, animated: true, completion: nil)
     
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //set the attributes
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  SecondViewController.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Class: This class is defined for statistic information. This class will allows users
//         to select historical data for all parking lots.
//  Created by MOLLY BIN on 2016-10-29.
//  Edited by Molly Bin, Qassam Yomok
//  Bugs: no known bugs
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var campus = ["Burnaby", "Vancouver", "Surrey"]
    var parkingLot = ["Burnaby": ["South Parking", "East Parking", "North Parking", "West Parkade", "Central Parkade","South Parkade"],"Vancouver":["P1", "P2", "P3"],"Surrey":["P1","P2","P3"]]
    //variables
    @IBOutlet weak var dateDisplay: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    let dateFormatter = NSDateFormatter()
    //let parkingLotData = CampusData()
     var statData = StatisticData()
    @IBOutlet weak var ParkingLotsPicker: UIPickerView!
    var selectedValue = ""
    var Date = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ParkingLotsPicker.dataSource = self
        ParkingLotsPicker.delegate = self
        
    }

    //functions that set the date
    @IBAction func datePickerChanged(sender: AnyObject) {
        setDate()
    }
    func setDate(){
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateDisplay.text = dateFormatter.stringFromDate(datePicker.date)
        Date = dateDisplay.text!
        setupStathttp()
    }
    
    
    //functions for UIPickerViewDelegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    //functions for UIPickerViewDelegate
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(component == 0){
            return campus.count
        }else{
            if(parkingLot[selectedValue] != nil){
                print(parkingLot[selectedValue]!.count)
                return parkingLot[selectedValue]!.count
            }else{
                return 0
            }
        }
    }
    
    //functions for UIPickerViewDelegate
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(component==0){
            return campus[row]
        }else{
            if(parkingLot[selectedValue] != nil){
                return parkingLot[selectedValue]![row]
            }else{
                return"?"
            }
        }
    }
    
    //functions for UIPickerViewDelegate
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(component == 0){
            selectedValue = campus[row]
            ParkingLotsPicker.reloadComponent(1)
            ParkingLotsPicker.selectRow(0, inComponent: 1, animated: true)

        }
  
        
    }
    
    @IBAction func toShowChart(sender: AnyObject) {
        //setupStathttp()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func setupStathttp(){
        //HTTP stuff
        
        statData.Data.removeAll()
        
        
        var cou = 0
        var month = ""
        var day = ""
        var year = ""
        
        for (i, element) in Date.characters.enumerate() {
            if ( element == "/"){
                  cou = cou + 1
            }
            else if (cou == 0 ){
                month.append(element)
            }
            else if (cou == 1){
                day.append(element)
        
            }
            else{
                year.append(element)
            
            }
        }
        print(Date)
        var m = (Double (month)!)
        var y = (Double (year)!)
        if (m >= 3){
           m = m - 2
        }
        else if ( m == 1 ){
            m = 11
            y = y - 1
        }
        else if ( m == 2 ){
           m = 12
           y = y - 1
        }
        else{
        }
        var d = (Double (day)!)
        var c = 20.0
        var x = floor(((2.6 * m ) - 0.2 ))
        var s = y + floor((y/4)) + floor((c/4))
        
        var w = ( (( d + x)  - (2 * c) + s ) % 7 )
        var WeekDay = ""
        if ( w == 0){
            WeekDay = "Sunday"
        }
        else if ( w == 1){
            WeekDay = "Monday"
        }
        else if ( w == 2){
            WeekDay = "Tuesday"
        }
        else if ( w == 3){
            WeekDay = "Wednesday"
        }
        else if ( w == 4){
            WeekDay = "Thursday"
        }
        else if ( w == 5){
            WeekDay = "Friday"
        }
        else{
            WeekDay = "Saturday"
        }
        let requestURL: NSURL = NSURL(string: "http://9f23a172.ngrok.io/data.json")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                do{
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                    print(WeekDay)
                    if let parkinglots = json["parking lots"] as? [[String: AnyObject]]{
                        //if let stt = json["Statistics"] as? [[String: AnyObject]]{
                        for parkinglot in parkinglots{
                            if let stat = parkinglot["Statistics"]as? [[String: AnyObject]]{
                                for parkinglot in stat   {
                                    if let Fri = parkinglot[WeekDay] as? [[String: AnyObject]]{
                                        for parkinglot in Fri{
                                            if let sdata = parkinglot["00"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["01"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["02"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["03"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["04"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["05"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["06"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["07"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["08"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["09"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["10"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["11"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["12"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["13"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["14"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["15"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["16"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["17"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["18"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["19"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["20"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["21"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["22"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                            if let sdata = parkinglot["23"]as? String{
                                                self.statData.Data.append(Int(sdata)!)
                                            }
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }catch {
                    print("Error with Json: \(error)")
                }
            }
            
        }
        task.resume()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "gotoChart"){
            let stac = segue.destinationViewController as! CharViewController
            stac.statData = statData
            print(self.statData.Data)
        }
    }


}


//
//  httpController.swift
//  ParkSFU
//  Group 6: QMRSolution
//
//  Created by MOLLY BIN on 2016-11-02.
//  Edited by Molly Bin, Qassam Yomok
//  Bugs: no known bugs
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//


import UIKit

class httpController: NSObject {
    var delegate: httpProtocol?
    func onSearch(url: String){
        
        var urlPath:NSURL? = NSURL(string:url)
        var request = NSURLRequest(URL: urlPath!)
        var error: NSError?
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {
            (response, data, error) -> Void in
            
            let resultData: NSDictionary! = try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
             NSLog("\(data)")
            
            
            self.delegate!.didReResults(resultData)
            //NSLog("lalalalalalalla: \(resultData)")
        })
    }
}
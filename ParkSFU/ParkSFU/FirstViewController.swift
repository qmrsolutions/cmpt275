//
//  FirstViewController.swift
//  ParkSFU
//  Group 6: QMRSolution
//  Class  This Class is a Home ViewController that allow users to
//         select three different campus where they are going to visit
//         This class will contains three buttons which will print selected
//         campus when user clicks.
//  Created by MOLLY BIN on 2016-10-29.
//  Edited by Molly Bin, Qassam Yomok, Dana Sy

//  Bugs: no known bugs
//  Copyright © 2016 MOLLY BIN. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var HomeView: UIImageView!
    @IBOutlet weak var BurnabyButton: UIButton!
    let roadreportdata = RoadReportData()
    var parkinglotdata = ParkingLotData()
    var statData = StatisticData()
    
    var emerencyNote=""
    var availability = ""
    
    override func viewWillAppear(animated: Bool) {
        setupburnabyParkinghttp()
    }    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setburnabyroadreporthttp()
        setupburnabyParkinghttp()
        setupEmerencyNotehttp()
    }

    /* Burnaby function */
    @IBAction func toBurnaby(sender: UIButton) {
        print("go to burnaby")
        setupEmerencyNotehttp()
    }
    
    /* Vancouver function */
    @IBAction func toVancouver(sender: UIButton) {
        print("go to Vancouver")
    }
    
    /* Surrey function */
    @IBAction func toSurrey(sender: UIButton) {
        print("go to Surrey")
    }
    
    /* JSON function to GET and parse Burnaby road report information from SFU Hub API */
    func setburnabyroadreporthttp(){
        //HTTP stuff
        let requestURL: NSURL = NSURL(string: "http://www.sfu.ca/security/sfuroadconditions/api/2/current")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                do{
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)

                    if let dict = json as? [String: AnyObject]{
                        
                        print(dict["conditions"]!["burnaby"]!!["roads"])
                        self.roadreportdata.broad = (dict["conditions"]!["burnaby"]!!["roads"]!!["status"] as? String)!
                        self.roadreportdata.adjroad = (dict["conditions"]!["burnaby"]!!["adjacentroads"]!!["status"] as? String)!
                        self.roadreportdata.classexam = (dict["conditions"]!["burnaby"]!!["classes_exams"]!!["status"] as? String)!
                        self.roadreportdata.transbus = (dict["conditions"]!["burnaby"]!!["transit"]!!["status"] as? String)!
                        //print(self.BurnabyRoadCond.text)
                        print(self.roadreportdata.broad)
                    }
                    
                }catch {
                    print("Error with Json: \(error)")
                }
            }
        }
        
        task.resume()
    }
    
    /* JSON function to GET and parse Burnaby lot information from webserver */
    func setupburnabyParkinghttp(){

        let requestURL: NSURL = NSURL(string: "http://9f23a172.ngrok.io/data.json")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            if (statusCode == 200) {
                do{
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                    self.parkinglotdata.lotName.removeAll()
                    self.parkinglotdata.FreeSpot.removeAll()
                    self.parkinglotdata.GPS.removeAll()
                    //  print(json)
                    if let parkinglots = json["parking lots"] as? [[String: AnyObject]]{
                        for parkinglot in parkinglots{
                            if let name = parkinglot["lot"] as? String{
                                if let free = parkinglot["free"] as? String
                                {
                                    if let gps = parkinglot["gps_location"] as? String{
                                        //print(name, free)
                                        
                                        self.parkinglotdata.lotName.append(name)
                                        self.parkinglotdata.FreeSpot.append(free)
                                        self.parkinglotdata.GPS.append(gps)
                                        print(self.parkinglotdata.lotName)
                                        print(self.parkinglotdata.FreeSpot)
                                        print(self.parkinglotdata.GPS)
                                    }


                                }
                            }
                        }
                    }
                }catch {
                    print("Error with Json: \(error)")
                }
            }
            
        }
        task.resume()
    }
    
    /* JSON function to GET and parse Burnaby emergency information from webserver */
    func setupEmerencyNotehttp(){
        //HTTP stuff
        let requestURL: NSURL = NSURL(string: "http://9f23a172.ngrok.io/emergency.json")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                do{
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                    
                    if let emergency = json["available"] as? String{
                        if (emergency == "true"){
                            if let Message = json["message"] as? String {
                                self.emerencyNote = Message
                                self.availability = emergency
                                

                            }
                        }
                    }
                }catch {
                    print("Error with Json: \(error)")
                }
            }
            
        }
        task.resume()
    }
    
    /* JSON function to GET and parse Burnaby parking statistics information from webserver */
    func setupStathttp(){
        //HTTP stuff
        let requestURL: NSURL = NSURL(string: "http://9f23a172.ngrok.io/data.json")!
        // let requestURL: NSURL = NSURL(string: "http://www.learnswiftonline.com/Samples/subway.json")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                do{
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                    if let parkinglots = json["parking lots"] as? [[String: AnyObject]]{
                        //if let stt = json["Statistics"] as? [[String: AnyObject]]{
                            for parkinglot in parkinglots{
                                if let stat = parkinglot["Statistics"]as? [[String: AnyObject]]{
                                    for parkinglot in stat   {
                                        if let Fri = parkinglot["Friday"] as? [[String: AnyObject]]{
                                            for parkinglot in Fri{
                                                if let sdata = parkinglot["00"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["01"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["02"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["03"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["04"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["05"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["06"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["07"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["08"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["09"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["10"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["11"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["12"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["13"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["14"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["15"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["16"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["17"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["18"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["19"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["20"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["21"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["22"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                                if let sdata = parkinglot["23"]as? String{
                                                    self.statData.Data.append(Int(sdata)!)
                                                }
                                            }
                                        }
                                            
                                        
                                    }
                                }
                            }
                        }
                }catch {
                    print("Error with Json: \(error)")
                }
            }
            
        }
        task.resume()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if(segue.identifier == "gotoBurnaby"){
            let svc = segue.destinationViewController as! BurnabyViewController
            
            svc.roadreportdata = roadreportdata
            svc.parkinglotdata = parkinglotdata
            svc.Availability = availability
            svc.EmergencyNote = emerencyNote
        }
    }

}

